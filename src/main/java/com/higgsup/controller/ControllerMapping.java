package com.higgsup.controller;/*
  By Chi Can Em  02-01-2018
 */

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerMapping {
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    protected ModelAndView login() throws Exception {
        ModelAndView modelAndView = new ModelAndView("Login");
        return modelAndView;
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    protected ModelAndView home() throws Exception {
        ModelAndView modelAndView = new ModelAndView("Home");
        return modelAndView;
    }
}
